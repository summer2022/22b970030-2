Name: openstack-helm
Version: ced30abead0bddb528d0c5fb7c1627dd8f1e22ba
Release: 0
Summary: The goal of OpenStack-Helm is to enable deployment, maintenance, and upgrading of loosely coupled OpenStack services and their dependencies individually or as part of complex environments.

License: ASL 2.0
URL: https://opendev.org/openstack/openstack-helm
Source0: openstack-helm-%{version}.tar.gz

Source1: openstack-helm.sh

BuildArch: noarch

Requires: openstack-helm

%description
The goal of OpenStack-Helm is to enable deployment, maintenance, and upgrading of loosely coupled OpenStack services and their dependencies individually or as part of complex environments.

%prep
%setup -q -n openstack-helm-%{version}

%build

%install
install -d -m755 %{buildroot}%{_datadir}/openstack-helm
install -D -m755 %{SOURCE1} %{buildroot}%{_bindir}/openstack-helm
cp -vr %{_builddir}/openstack-helm-%{version} %{buildroot}%{_datadir}/openstack-helm/openstack-helm

%files
%{_datadir}/openstack-helm/openstack-helm
%{_bindir}/openstack-helm

%changelog
* Tue Aug 30 2022 Yinuo Deng <i@dyn.im> - ced30abead0bddb528d0c5fb7c1627dd8f1e22ba-0
- Initial version
#!/bin/bash

set -e

OPENSTACK_HELM='/usr/share/openstack-helm/openstack-helm'
OPENSTACK_HELM_INFRA='/usr/share/openstack-helm/openstack-helm-infra'

function menu {
    echo "Welcome to OpenStack-Helm installation program for openEuler. I will guide you through the installation. "
    echo "Please refer to https://docs.openstack.org/openstack-helm/latest/ to get more information about OpenStack-Helm. "
    echo "We recommend doing this on a new bare metal or virtual OS installation. "

    while :; do
        echo -e "\n"
        echo "Now you have the following options: "
        echo "i: Start automated installation"
        echo "c: Check if all pods in Kubernetes are working"
        echo "e: Exit"

        read -p "Your choice? [i/c/e]: " x
        case $x in
            [Ii]* ) install;;
            [Ee]* ) exit 0;;
            [Cc]* ) check;;
            * ) echo "Input invalid.";;
        esac
    done
}

function install {
    while :; do
        echo -e "\n"
        echo "There are two storage backends available for OpenStack-Helm: NFS and CEPH. Which storage backend would you like to use? "
        echo "n: NFS storage backend"
        echo "c: CEPH storage backend"
        echo "b: Go back to parent menu"

        read -p "Your choice? [n/c/b]: " x
        case $x in
            [Nn]* ) install_nfs;;
            [Cc]* ) install_ceph;;
            [Bb]* ) menu;;
            * ) echo "Input invalid.";;
        esac
    done
}

function install_nfs {
    cd $OPENSTACK_HELM
    echo "I will start installing OpenStack-Helm with NFS as storage backend to your system. Please press ENTER to continue. "
    read -p ""

    set -x

    ./tools/deployment/developer/common/010-deploy-k8s.sh
    ./tools/deployment/developer/common/020-setup-client.sh
    ./tools/deployment/developer/common/030-ingress.sh
    ./tools/deployment/developer/nfs/040-nfs-provisioner.sh
    ./tools/deployment/developer/nfs/050-mariadb.sh
    ./tools/deployment/developer/nfs/060-rabbitmq.sh
    ./tools/deployment/developer/nfs/070-memcached.sh
    ./tools/deployment/developer/nfs/080-keystone.sh
    ./tools/deployment/developer/nfs/090-heat.sh
    ./tools/deployment/developer/nfs/100-horizon.sh
    ./tools/deployment/developer/nfs/120-glance.sh
    ./tools/deployment/developer/nfs/140-openvswitch.sh
    ./tools/deployment/developer/nfs/150-libvirt.sh
    ./tools/deployment/developer/nfs/160-compute-kit.sh
    ./tools/deployment/developer/nfs/170-setup-gateway.sh

    set +x

    echo "Installation complete!"
}


function install_ceph {
    cd $OPENSTACK_HELM
    echo "I will start installing OpenStack-Helm with CEPH as storage backend to your system. Please press ENTER to continue. "
    read -p ""

    set -x

    ./tools/deployment/developer/common/010-deploy-k8s.sh
    ./tools/deployment/developer/common/020-setup-client.sh
    ./tools/deployment/developer/common/030-ingress.sh
    ./tools/deployment/developer/ceph/040-ceph.sh
    ./tools/deployment/developer/ceph/050-mariadb.sh
    ./tools/deployment/developer/ceph/060-rabbitmq.sh
    ./tools/deployment/developer/ceph/070-memcached.sh
    ./tools/deployment/developer/ceph/080-keystone.sh
    ./tools/deployment/developer/ceph/090-heat.sh
    ./tools/deployment/developer/ceph/100-horizon.sh
    ./tools/deployment/developer/ceph/120-glance.sh
    ./tools/deployment/developer/ceph/140-openvswitch.sh
    ./tools/deployment/developer/ceph/150-libvirt.sh
    ./tools/deployment/developer/ceph/160-compute-kit.sh
    ./tools/deployment/developer/ceph/170-setup-gateway.sh

    set +x

    echo "Installation complete!"
}

function check {
    echo "Showing pods that are NOT running or completed: "
    echo "STAUTS NAME NAMESPACE"
    kubectl get pods -A -o json | jq -r \
        '.items[] | .status.phase + " " + .metadata.name + " " + .metadata.namespace' | grep -v \
        -e "Running" -e "Succeeded"
}

if [ ! -f /etc/os-release ]; then
    echo "Cannot determine current distribution. "
    exit 1
fi

. /etc/os-release
HOST_OS=${HOST_OS:="${ID}"}
if [ ! "$ID" == "openEuler" ]; then
    echo "This installation program is for openEuler only. "
    exit 1
fi

if [ ! -d $OPENSTACK_HELM ] || [ ! -d $OPENSTACK_HELM_INFRA ]; then
    echo "Source file not ready, please reinstall the RPM package. "
    exit 1
fi

menu